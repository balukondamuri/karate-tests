# Karate-Tests
This project contains the karate tests that runs spring boot application and test the API's.

### To run the spring application before run the tests
* Open Terminal/Command Line and navigate to the ```src/main/resources/```
* Run ``` java -jar employee-application.jar ```

### Running the tests
* ``` gradle build (or) gradle test ``` command will run the tests in default application port 8080
