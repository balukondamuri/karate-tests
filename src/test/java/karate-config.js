function () {
  var env = karate.env;
  karate.log('karate.env is :', env)
  if (!env) {
    env = 'local'
  }
  var config = {
    baseUrl: ''
  };
  if (env == 'local') {
    config.baseUrl = 'http://localhost:8080'
  } else if (env == 'qa') {
    config.baseUrl = 'http://localhost:8089'
  } else if (env == 'dev') {
    config.baseUrl = 'http://localhost:8080'
  }
  karate.configure('connectTimeout', 5000);
  karate.configure('readTimeout', 5000);
  return config;
}