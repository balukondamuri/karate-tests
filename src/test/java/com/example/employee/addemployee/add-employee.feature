Feature: Add employee tests

  Background: Prerequisit for this feature is adding employess before tests
    Given url baseUrl
    * def results = callonce read('add-employee-callonce.feature') {fName:'Balu',lName:'Kane',dName:'IT',role:'Sr Engineer'}
    * print 'result ', results


  Scenario: makesure employee added to the system without any issues
    * header Authorization = call read('classpath:user-auth.js') {username:'user',password:'user'}
    * def id = $results.response.id
    * print 'id ', id
    Given path 'employee/',id
    When method GET
    Then status 200
