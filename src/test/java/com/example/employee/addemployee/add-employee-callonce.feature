Feature: Adding Employee
  this is called feature for adding employee
  Background: Authorize Service
  Given url baseUrl
  * header Authorization = call read('classpath:user-auth.js') {username:'admin', password:'admin'}

  Scenario: Add Employee
    Given path 'employee/add'
    And request
      """
      {
      "firstName":'#(fName)',
      "lastName":'#(lName)',
      "departmentName":'#(dName)',
      "role":'#(role)'
      }
      """
    When method POST
