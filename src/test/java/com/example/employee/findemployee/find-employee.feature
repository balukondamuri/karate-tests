Feature: find employee

   This feature file should be verify and validate the find employee functionality

   Background: connect to the application
      Given url baseUrl


   Scenario: search for an emaployee based on the id with valid user
      * header Authorization = call read('classpath:user-auth.js') {username:'admin',password:'admin'}
      Given path 'employee/add'
      And request
         """
         {
            "firstName": "John",
            "lastName": "Smith",
            "departmentName": "IT",
            "role": "Sr Engineer"
         }
         """
      When method POST
      * def id = response.id
      * header Authorization = call read('classpath:user-auth.js') {username:'user',password:'user'}
      And path 'employee/',id
      When method GET
      Then status 200
      And match $.id == "#number"
      And match $.firstName == "#string"
      And match $.lastName == "#string"
      And match $.departmentName == "#string"
      And match $.role == "#string"


   Scenario: search for an emaployee based on the id with invalid user
      * header Authorization = call read('classpath:user-auth.js') {username:'admin',password:'admin'}
      Given path 'employee/add'
      * def body = read('employee.json')
      And request body
      When method POST
      * def id = response.id
      * header Authorization = call read('classpath:user-auth.js') {username:'user',password:'admin'}
      And path 'employee/',id
      When method GET
      Then status 401

