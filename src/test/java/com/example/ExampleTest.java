package com.example;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import com.intuit.karate.junit4.Karate;

import org.junit.Test;
import org.junit.runner.RunWith;

import karate.rest.soap.testing.ReportUtility;

@RunWith(Karate.class)
public class ExampleTest{

  
  //@Test
  public void runReport(){
    ReportUtility.generateReport(ReportUtility.testParallel());
    Results result = Runner.parallel(getClass(),5,"target/surefire-reports");
    assertTrue(result.getErrorMessages(),result.getFailCount()==0);
  
  }
}

