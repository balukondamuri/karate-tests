function (userDetails){
  var authUtil = Java.type('karate.rest.soap.testing.AuthUtility');
  var auth = authUtil.basicAuthEncoding(userDetails.username,userDetails.password);
  return auth;
}